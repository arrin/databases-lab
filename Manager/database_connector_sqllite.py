from functools import partial
from Manager.sqllite_connector import storage
from Manager.entities import User
from Manager.entities import Equipment
from Manager.entities import UserEquipment
from Manager.entities import Permissions
from Manager.entities import Record


def GetUserProfile(user_id):
    user_id = int(user_id)
    db = storage.get_storage()
    row = db.GetUserProfile(user_id)
    (id, nickname, first, second, address, phone, date_hired, date_fired, position, permissions, boss_id) = row

    boss_row = db.GetUserProfile(boss_id)

    return User(
        id,
        nickname,
        first,
        second,
        address,
        phone,
        date_hired,
        date_fired,
        db.GetPositionByName(position)[1],
        permissions,
        boss_row[1] if boss_row is not None else None
    )


def _row_to_history(row):
    (date, record) = row
    return Record(date, record)


def GetUserHistory(user_id):
    user_id = int(user_id)

    db = storage.get_storage()
    raw_list = db.GetHistory(user_id)
    result = map(_row_to_history, raw_list)

    return result


def HireUser(user):
    user.permissions = user.permissions.value
    storage.get_storage().HireEmployee(user)


def MoveUser(user):
    # user.position = user.permissions.value
    storage.get_storage().MoveEmployee(user)


def FireUser(user_id):
    user_id = int(user_id)
    storage.get_storage().FireEmployee(user_id)


def _row_to_user(row, boss_username):
    (id, nickname, first, second, address, phone, date_hired, date_fired, position, permissions, boss_id) = row

    return User(
        id,
        nickname,
        first,
        second,
        address,
        phone,
        date_hired,
        date_fired,
        position,
        # storage.get_storage().GetPositionById(position)[1],
        permissions,
        boss_username
    )


def GetUsers(boss_id = None):
    boss_id = int(boss_id) if boss_id is not None else None
    db = storage.get_storage()

    if (boss_id is None):
        raw_list = db.GetAllUsers()
        func = partial(_row_to_user, boss_username=None)
        result = list(map(func, raw_list))
        for item in result:
            item.position = storage.get_storage().GetPositionById(item.position)[1]
        return result

    boss = GetUserProfile(boss_id)

    raw_list = db.GetUsers(boss_id)
    func = partial(_row_to_user, boss_username = boss.username)
    users_list = map(func, raw_list)

    return users_list


def GetUserProfileByUsername(username):
    db = storage.get_storage()
    row = db.GetUserByName(username)

    (id, nickname, first, second, address, phone, date_hired, date_fired, position, permissions, boss_id) = row

    return User(
        id,
        nickname,
        first,
        second,
        address,
        phone,
        date_hired,
        date_fired,
        position,
        Permissions[permissions.upper()],
        GetUserProfile(boss_id).username if boss_id is not None else None
    )


def _row_to_equipment(row):
    (id, model, count, count_rest) = row

    return Equipment(id, model, count, count_rest)


def GetAllEquipment():  # add rest count for equipment
    db = storage.get_storage()
    raw_list = db.GetAllEquipmentWithCountRest()

    func = _row_to_equipment
    equipment_list = map(func, raw_list)

    return equipment_list


def GetEquipment(equip_id):
    equip_id = int(equip_id)
    db = storage.get_storage()
    row = db.GetEquipmentById(equip_id)
    return _row_to_equipment(row)


def _row_to_user_equipment(row):
    (id, date_given, reason, model) = row
    return UserEquipment(id, model, reason, date_given)


def GetOwnedEquipment(user_id):
    user_id = int(user_id)
    db = storage.get_storage()
    raw_list = db.GetOwnedEquipment(user_id)

    func = _row_to_user_equipment
    user_equipment_list = map(func, raw_list)

    return user_equipment_list


def GiveEquipment(user_id, equip_id, reason):
    Log(user_id, "Took new equipment {}".format(equip_id))
    equip_id = int(equip_id)
    user_id = int(user_id)
    db = storage.get_storage()
    db.GiveEquipment(equip_id, user_id, reason)


def ReturnEquipment(equip_id):
    equip_id = int(equip_id)
    db = storage.get_storage()
    db.ReturnEquipment(equip_id)


def CreateEquipment(equipment):
    db = storage.get_storage()
    db.AddNewEquipmentToStorage(equipment)


def RemoveEquipment(equip_id):
    equip_id = int(equip_id)
    db = storage.get_storage()
    db.RemoveEquipmentFromStorage(equip_id, 1)


def IncrementEquipment(equip_id):
    equip_id = int(equip_id)
    db = storage.get_storage()
    db.AddExistingEquipmentToStorage(equip_id, 1)


def Log(user_id, message):
    db = storage.get_storage()
    db.Log(user_id, message)