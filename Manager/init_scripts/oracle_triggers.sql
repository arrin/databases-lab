create or replace procedure CreateLogRecord(message varchar2) is
begin
    insert into Records(EmployeeId, RecordContent, DatetimeCreated)
    values(1, message, (select CURRENT_DATE from DUAL));
end;

-- EMPLOYEES DML TRIGGERS

-- THIS TRIGGER ONLY IN PRODUCTION
create or replace trigger EmployeesDenyDeleteTrigger before delete on Employees
declare
    delete_not_allowed Exception;
begin
    CreateLogRecord('There was attempt to delete a user');
    raise delete_not_allowed;
end;

create or replace trigger EmployeesInsertTrigger after insert on Employees for each row
begin
    CreateLogRecord('A new user was created, ID=' || :new.ID || '; NICKNAME=' || :new.NICKNAME);
end;

create or replace trigger EmployeesUpdateTrigger after update on Employees for each row
begin
    CreateLogRecord('User was updated, ID=' || :new.ID || '; NICKNAME=' || :new.NICKNAME);
end;

-- EQUIPMENT DML TRIGGERS

create or replace trigger EquipmentDeleteTrigger before delete on Equipment for each row
begin
    CreateLogRecord('Equipment was deleted, ID=' || :old.ID || '; MODEL=' || :old.MODEL);
end;

create or replace trigger EquipmentInsertTrigger after insert on Equipment for each row
begin
    CreateLogRecord('New equipment was created, ID=' || :old.ID || '; MODEL=' || :old.MODEL);
end;

create or replace trigger EquipmentUpdateTrigger after update on Equipment for each row
begin
    CreateLogRecord('Equipment was updated, ID=' || :old.ID || '; MODEL=' || :old.MODEL);
end;

-- GIVENEQUIPMENT DML TRIGGERS

-- THIS TRIGGER ONLY IN PRODUCTION
create or replace trigger GivenEquipmentDenyDeleteTrigger before delete on GivenEquipment for each row
begin
    CreateLogRecord('GivenEquipment deleted, ID=' || :old.ID || '; UserID=' || :old.EmployeeId || '; EqId=' || :old.EquipmentId);
end;

create or replace trigger GivenEquipmentInsertTrigger after insert on GivenEquipment for each row
begin
    CreateLogRecord('GivenEquipment created, ID=' || :old.ID || '; UserID=' || :old.EmployeeId || '; EqId=' || :old.EquipmentId);
end;

create or replace trigger GivenEquipmentUpdateTrigger after update on GivenEquipment for each row
begin
    CreateLogRecord('GivenEquipment updated, ID=' || :old.ID || '; UserID=' || :old.EmployeeId || '; EqId=' || :old.EquipmentId);
end;