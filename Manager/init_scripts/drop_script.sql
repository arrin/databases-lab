drop view AllEmployeesView;
drop view GivenEquipmentGrouped;
drop table Records;
drop table GivenEquipment;
drop table Equipment;
drop table Employees;
drop table Permissions;
drop table Positions;