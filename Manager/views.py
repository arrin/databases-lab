from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from Manager import database_connector
from Manager import forms, entities
from Manager import utils
from Manager.exceptions_handler import exceptions_handler

def add_superuser():
    try:
        u = User(username="admin")
        u.save()
        u.set_password("admin")
        u.save()
    except:
        pass

def main_page(request):
    add_superuser()
    # t = storage.get_storage()
    return actions(request)


@login_required
@exceptions_handler
def actions(request):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    template = loader.get_template('actions.html')

    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    context = {'equip' : True, 'people' : True, 'user_id': user_id, 'boss_id': boss_id}
    return HttpResponse(template.render(context))


@exceptions_handler
@login_required
def dissmiss(request, id):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    cur_user = request.user.username
    database_connector.FireUser(id, user_id, cur_user)
    return redirect(show_people)


@exceptions_handler
@login_required
def my_profile(request, id):    # my profile view
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    init_vals = vars(database_connector.GetUserProfile(id))
    form = forms.ViewPersonForm(initial=init_vals)
    equip = database_connector.GetOwnedEquipment(user_id)
    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    return render(request, 'profile.html', {'form': form, 'user_id': user_id, 'equip': equip,
                                            'history': database_connector.GetUserHistory(user_id),
                                            'boss_id': boss_id})


@exceptions_handler
@login_required
def edit_profile(request, id):    # edit profile by manager
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    if request.method == "POST":
        form = forms.EditPersonForm(request.POST)
        if form.is_valid():
            user = database_connector.GetUserProfile(id)
            user.boss_username = form.cleaned_data['boss_username']
            user.position = form.cleaned_data['position']
            database_connector.MoveUser(user, user_id, request.user.username)
        return redirect(actions)
    else:
        init_vals = vars(database_connector.GetUserProfile(id))
        form = forms.EditPersonForm(initial=init_vals)

    equip = database_connector.GetOwnedEquipment(id)
    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    return render(request, 'profile.html', {'form': form, 'user_id': user_id, 'equip': equip,
                                            'is_change_equip': utils.is_admin(user_id) or utils.is_equip_worker(user_id),
                                            'is_update_profile': utils.is_admin(user_id) or utils.is_hr(user_id).bit_length(),
                                            'history': database_connector.GetUserHistory(id),
                                            'boss_id': boss_id})


@exceptions_handler
@login_required
def create_user(request):    # create new user
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    user = entities.User(None, "default", "default", "default", "-", "-",
                         None, None, "-", entities.Permissions.WORKER, None)
    if request.method == "POST":
        form = forms.CreatePersonForm(request.POST)
        if form.is_valid():
            user.username = form.cleaned_data['username']
            user.first_name = form.cleaned_data['first_name']
            user.second_name = form.cleaned_data['second_name']
            user.phonenumber = form.cleaned_data['phonenumber']
            user.home_address = form.cleaned_data['home_address']
            user.permissions = entities.Permissions(int(form.cleaned_data['status']))
            user.position = form.cleaned_data['position']
            user.boss_username = form.cleaned_data['boss_username']

            if User.objects.filter(username=user.username).exists():
                equip = database_connector.GetOwnedEquipment(user_id)
                boss_id = database_connector.GetUserProfile(user_id).boss_username
                boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None

                return render(request, 'profile.html', {'form': form, 'user_id': user_id, 'equip': equip,
                                                        'is_update_profile' : True,
                                                        'boss_id': boss_id})

            u = User(username=user.username)
            u.save()
            u.set_password(user.username)
            u.save()

            database_connector.HireUser(user, user_id, request.user.username)
        return redirect(actions)
    else:
        init_vals = vars(user)
        form = forms.CreatePersonForm(initial=init_vals)

    equip = database_connector.GetOwnedEquipment(user_id)
    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    return render(request, 'profile.html', {'form': form, 'user_id': user_id, 'equip': equip,
                                            'is_update_profile': True, 'boss_id': boss_id})


@exceptions_handler
@login_required
def profile(request, id):
    user = database_connector.GetUserProfile(id)
    if user.username == request.user.username:
        return my_profile(request, id)
    else:
        return edit_profile(request, id)


@exceptions_handler
@login_required
def show_people(request):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    template = loader.get_template('people.html')
    boss_id = user_id
    if not utils.is_worker(user_id):
        boss_id = None

    people = database_connector.GetUsers(boss_id)

    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    context = {'people': people, 'user_id': user_id, 'is_create': utils.is_admin(user_id) or utils.is_hr(user_id),
               'boss_id': boss_id}
    return HttpResponse(template.render(context))


# equip section
@exceptions_handler
@login_required
def show_equip(request):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    template = loader.get_template('equip_list.html')

    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    context = {'equips': database_connector.GetAllEquipment(), 'user_id': user_id,
               'is_equip_manager': utils.is_admin(user_id) or utils.is_equip_worker(user_id),
               'boss_id': boss_id}
    return HttpResponse(template.render(context))


@login_required
@exceptions_handler
def return_equip(request, equip_id):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    database_connector.ReturnEquipment(equip_id, user_id, request.user.username)
    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    return redirect(actions)

@exceptions_handler
@login_required
def remove_equip(request, equip_id):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id

    item = database_connector.GetEquipment(equip_id)
    if (item.count_free) >= 1:
        database_connector.RemoveEquipment(equip_id, user_id, request.user.username)

    return redirect(show_equip)



@login_required
@exceptions_handler
def inc_equip(request, equip_id):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    database_connector.IncrementEquipment(equip_id, user_id, request.user.username)
    return redirect(show_equip)


@login_required
@exceptions_handler
def create_equip(request):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    if request.method == "POST":
        form = forms.EquipmentForm(request.POST)
        if form.is_valid():
            equip = entities.Equipment(None, form.cleaned_data['name'], form.cleaned_data['count'])
            database_connector.CreateEquipment(equip, user_id, request.user.username)

        return redirect(show_equip)
    else:
        form = forms.EquipmentForm()

    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    return render(request, 'equip_form.html', {'form': form, 'user_id': user_id, 'boss_id': boss_id})


@login_required
@exceptions_handler
def give_equip(request, equip_id):
    user_id = database_connector.GetUserProfileByUsername(request.user.username).id
    if request.method == "POST":
        form = forms.GiveEquipmentForm(request.POST)
        if form.is_valid():
            target_id = database_connector.GetUserProfileByUsername(form.cleaned_data['username']).id
            reason = form.cleaned_data['reason']
            database_connector.GiveEquipment(equip_id, target_id, reason,
                                             user_id, request.user.username)  # slozhna!
        return redirect(show_equip)
    else:
        form = forms.GiveEquipmentForm()

    boss_id = database_connector.GetUserProfile(user_id).boss_username
    boss_id = database_connector.GetUserProfileByUsername(boss_id).id if boss_id is not None else None
    return render(request, 'equip_form.html', {'form': form, 'user_id': user_id, 'boss_id': boss_id})