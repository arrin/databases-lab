create or replace procedure set_current_user_nickname(nickname varchar2) as
begin
    dbms_session.set_context('CLIENTCONTEXT', 'user_session_nickname', nickname);
end set_current_user_nickname;

create or replace procedure set_current_user_id(id number) as
begin
    dbms_session.set_context('CLIENTCONTEXT', 'user_session_id', id);
end set_current_user_id;

create or replace function get_current_user_nickname return varchar2 as
    result_nickname varchar2(100);
begin
    select sys_context('CLIENTCONTEXT', 'user_session_nickname') into result_nickname from dual;
    return result_nickname;
end get_current_user_nickname;

create or replace function get_current_user_id return number as
    result_id number;
begin
    select sys_context('CLIENTCONTEXT', 'user_session_id') into result_id from dual;
    return result_id;
end get_current_user_id;

create or replace procedure CreateLogRecord(message varchar2) as
begin
    insert into Records(EmployeeId, RecordContent, DatetimeCreated)
    values(
        get_current_user_id(),
        ('[INFO] [done by user ' || get_current_user_nickname() || ']: ' || message),
        (select CURRENT_DATE from DUAL)
    );
end CreateLogRecord;