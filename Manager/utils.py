from Manager import database_connector
from Manager.entities import Permissions


def is_equip_worker(user_id):
    user = database_connector.GetUserProfile(user_id)
    return user.permissions == Permissions.EQUIPS.name.lower()

def is_admin(user_id):
    user = database_connector.GetUserProfile(user_id)
    return user.permissions == Permissions.ADMIN.name.lower()

def is_hr(user_id):
    user = database_connector.GetUserProfile(user_id)
    return user.permissions == Permissions.HR.name.lower()

def is_worker(user_id):
    user = database_connector.GetUserProfile(user_id)
    return user.permissions == Permissions.WORKER.name.lower()
