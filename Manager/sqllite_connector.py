import sqlite3
from Manager.entities import User, Permissions

DB_PATH = "database.db"

class storage:
    storage = None

    @classmethod
    def get_storage(cls):
        if cls.storage is None:
            cls.storage = storage(DB_PATH)

        return cls.storage


    def __init__(self, filepath):
        self.path = filepath
        # self.InitDatabase()


    def InitDatabase(self):
        self.drop_tables()
        self.create_tables()
        self.InitPositionsPermissions()
        superuser = User(None, "admin", "", "", "", "", None, None, "head", 1, None)
        self.HireEmployee(superuser)


    def _cursor(self):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        return cursor


    def _commit_transaction(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        cursor.execute(query, args)
        conn.commit()


    def _commit_many(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        cursor.executemany(query, args)
        conn.commit()

    def _get_one(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        return cursor.execute(query, args).fetchone()


    def _get_all(self, query, args):
        conn = sqlite3.connect(self.path)
        cursor = conn.cursor()
        return cursor.execute(query, args).fetchall()


    def create_tables(self):
        cursor = self._cursor()

        with open('Manager/init_scripts/init_positions.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_permissions.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_employees.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_equipment.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_given_equipment.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_records.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_view_all_employees.sql') as query:
            cursor.execute(query.read())

        with open('Manager/init_scripts/init_view_given_equipment_grouped') as query:
            cursor.execute(query.read())


    def drop_tables(self):
        cursor = self._cursor()

        with open('Manager/drop_script.sql', 'r') as drop_script:
            cursor.executescript(drop_script.read())


    def print_tables(self):
        cursor = self._cursor()

        table_names = cursor.execute("select * from sqlite_master where type = 'table'").fetchall()
        for table_name in table_names:
            print(table_name)


    def print_views(self):
        cursor = self._cursor()

        view_names = cursor.execute("select * from sqlite_master where type = 'view'").fetchall()
        for view_name in view_names:
            print(view_name)


    def InitPositionsPermissions(self):
        query = """
        
        insert into Positions(Id, Position)
        values(1, "admin");
        
        """

        self._commit_transaction(query, [])


        items = [(p.value, p.name.lower()) for p in Permissions]

        query = """

                insert into Permissions(Id, Permission)
                values(?, ?);

                """

        self._commit_many(query, items)


    def GetPositionById(self, position_id):
        query = """
        
        select *  from Positions
        where Id = ?
        
        """

        args = [
            position_id
        ]

        return self._get_one(query, args)


    def GetPositionById(self, position_id):
        query = """
        
        select * from Positions
        where Id = ?
        
        """

        args = [
            position_id
        ]

        return self._get_one(query, args)


    def GetPositionByName(self, position):
        query = """
        
        select * from Positions
        where Position = ?
        
        """

        args = [
            position
        ]

        result = self._get_one(query, args)

        if result is None:
            query = """
            
            insert into Positions(Position)
            values(?)
            
            """

            self._commit_transaction(query, args)

            return self.GetPositionByName(position)

        else:
            return result


    def HireEmployee(self, employee):
        query = """
        
        insert into Employees 
              (NickName, FirstName, SecondName, HomeAddress, Phone,  DateHired, DateFired, PositionId, PermissionId, BossId)
        values(       ?,         ?,          ?,           ?,     ?, date('now'),      null,          ?,            ?,      ?);
        
        """

        boss_id = self.GetUserByName(employee.boss_username)
        boss_id = boss_id[0] if boss_id is not None else None

        args = [
            employee.username,
            employee.first_name,
            employee.second_name,
            employee.home_address,
            employee.phonenumber,
            self.GetPositionByName(employee.position)[0],
            employee.permissions,
            boss_id,
        ]

        self._commit_transaction(query, args)

    def MoveEmployee(self, employee):
        query = """
        
        update Employees
        set 
            PositionId   = ?,
            BossId       = ?
        where
            Id = ?;
            
        """

        args = [
            self.GetPositionByName(employee.position)[0],
            self.GetUserByName(employee.boss_username)[0],
            employee.id
        ]

        self._commit_transaction(query, args)


    def FireEmployee(self, user_id):
        query = """
        
        update Employees
        set
            DateFired = date('now')
        where
            Id = ?;
            
        """

        args = [
            user_id
        ]

        self._commit_transaction(query, args)


    def AddNewEquipmentToStorage(self, equipment):
        query = """
        
        insert into Equipment 
	          (Model, Count)
        values(    ?,     ?);  
        
        """

        args = [
            equipment.model,
            equipment.count
        ]

        self._commit_transaction(query, args)


    def AddExistingEquipmentToStorage(self, equipment_id, count_added):
        query = """

                update Equipment
                set
                    Count = Count + ?
                where
                    Id = ?;

                """

        args = [
            count_added,
            equipment_id
        ]

        self._commit_transaction(query, args)


    def RemoveEquipmentFromStorage(self, equipment_id, count_reduced):
        query = """

                update Equipment
                set
                    Count = Count - ?
                where
                    Id = ?;

                """

        args = [
            count_reduced,
            equipment_id
        ]

        self._commit_transaction(query, args)


    def GiveEquipment(self, equipment_id, employee_id, reason):
        query = """
        
        insert into GivenEquipment 
              ( DatetimeGiven, DatetimeReturned, ReasonGiven, EquipmentId, EmployeeId)
        values(   date('now'),             null,           ?,           ?,          ?);
        
        """

        args = [
            reason,
            equipment_id,
            employee_id
        ]

        self._commit_transaction(query, args)


    def ReturnEquipment(self, equip_id):
        query = """
        
        update GivenEquipment
        set
            DatetimeReturned = date('now')
        where
            Id = ?;
        
        """

        args = [
            equip_id
        ]

        self._commit_transaction(query, args)


    def GetUserProfile(self, user_id):
        query = """
        
        select 
            worker.Id, 
            worker.NickName, 
            worker.FirstName, 
            worker.SecondName, 
            worker.HomeAddress, 
            worker.Phone, 
            worker.DateHired, 
            worker.DateFired,
            worker.Position,
            worker.Permission,
            worker.BossId
        
        from AllEmployeesView worker
        
        where 
            worker.Id = ?;
        
        """

        args = [
            user_id
        ]

        return self._get_one(query, args)


    def GetUserByName(self, username):
        query = """
        
        select 
            worker.Id, 
            worker.NickName, 
            worker.FirstName, 
            worker.SecondName, 
            worker.HomeAddress, 
            worker.Phone, 
            worker.DateHired, 
            worker.DateFired,
            worker.Position,
            worker.Permission,
            worker.BossId
        
        from AllEmployeesView worker
        
        where 
            worker.NickName = ?;
        
        """

        args = [
            username
        ]

        return self._get_one(query, args)


    def GetAllEquipment(self):
        query = """
        
        select
            Id,
            Model,
            Count
        
        from Equipment;

        
        """

        return self._get_all(query, [])


    def GetEquipmentById(self, equipment_id):
        query = """

        select
            eq.Id                  as Id,
            eq.Model               as Model,
            eq.Count               as Count,
            eq.Count - given.Count as CountRest

        from Equipment eq 
        left outer join GivenEquipmentGrouped given
        on eq.Id = given.EquipmentId

        where eq.Id = ?
        
        """

        args = [
            equipment_id
        ]

        return self._get_one(query, args)


    def GetOwnedEquipment(self, user_id):
        query = """
        
        select
            given.Id            as Id,
            given.DatetimeGiven as DatetimeGiven,
            given.ReasonGiven   as Reason,
            eq.Model            as Model
            
        from GivenEquipment given inner join Equipment eq
        on given.EquipmentId = eq.Id 
            
        where 
            given.EmployeeId = ? and given.DatetimeReturned is null
        
        """

        args = [
            user_id
        ]

        return self._get_all(query, args)


    def GetAllEquipmentWithCountRest(self):
        query = """
        
        select
            eq.Id                  as Id,
            eq.Model               as Model,
            eq.Count               as Count,
            eq.Count - given.Count as CountRest
            
        from Equipment eq 
        left outer join GivenEquipmentGrouped given
        on eq.Id = given.EquipmentId
            
        """

        return self._get_all(query, [])


    def GetAllUsers(self):
        query = """select * from Employees;"""

        return self._get_all(query, [])


    def GetUsers(self, boss_id):
        query = """
      
        select 
            worker.Id, 
            worker.NickName, 
            worker.FirstName, 
            worker.SecondName, 
            worker.HomeAddress, 
            worker.Phone, 
            worker.DateHired, 
            worker.DateFired,
            worker.Position,
            worker.Permission,
            worker.BossId
        
        from AllEmployeesView worker
        
        where 
            worker.BossId = ?;
        
        """

        args = [
            boss_id
        ]

        return self._get_all(query, args)


    def Log(self, user_id, message):
        query = """
        
        insert into Records (RecordContent, DatetimeCreated, EmployeeId)
        values              (            ?,     date('now'),          ?)
        
        """

        args = [
            message,
            user_id
        ]

        self._commit_transaction(query, args)


    def GetHistory(self, user_id):
        query = """
        
        select DatetimeCreated, RecordContent 
        from Records
        where EmployeeId = ?
        
        """

        args = [
            user_id
        ]

        return self._get_all(query, args)