from enum import Enum


class UserEquipment:
    def __init__(self, id, name, reason, date_given):
        self.id = id
        self.name = name
        self.reason = reason
        self.date_given = date_given

    def __str__(self):
        return ('{} {}'.format(self.name, self.reason))


class Equipment:
    def __init__(self, id, model, count, count_free=None):
        self.id = id
        self.model = model
        self.count = count
        self.count_free = count_free if count_free is not None else count

    def __str__(self):
        return ('{} {}'.format(self.id, self.model))


class User:
    def __init__(self,
        id=None,
        username=None,
        first_name=None,
        second_name=None,
        home_address=None,
        phonenumber=None,
        date_hired=None,
        date_fired=None,
        position=None,   # stores position string name
        permissions=None,
        boss_username=None
    ):

        self.id = id
        self.username = username
        self.first_name = first_name
        self.second_name = second_name
        self.home_address = home_address
        self.phonenumber = phonenumber
        self.date_hired = date_hired
        self.date_fired = date_fired
        self.position = position
        self.permissions = permissions if permissions is not None else Permissions.WORKER
        self.boss_username = boss_username

    def __str__(self):
        return ('{} {}  boss: {}, pos: {}'\
                .format(self.id, self.username, self.boss_username, self.position))


class Permissions(Enum):
    ADMIN  = 1
    WORKER = 2
    HR     = 3
    EQUIPS = 4


class Record:
    def __init__(self, time, record):
        self.time = time
        self.record = record

    def __str__(self):
        return '{} {}'.format(self.time, self.record)