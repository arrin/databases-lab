from django.conf.urls import url
from Manager import views, forms

urlpatterns = [
    url(r'^$', views.main_page, name='home_page'),
    url(r'^actions$', views.actions, name='actions'),
    url(r'^people$', views.show_people),
    url(r'^profile/(?P<id>[0-9A-Fa-f]+)/$', views.profile),
    url(r'^person/dissmiss/(?P<id>[0-9A-Fa-f]+)/$', views.dissmiss),
    url(r'^person/create_user$', views.create_user),

    url(r'^login.html$', forms.LoginFormView.as_view()),
    url(r'^logout', forms.LogoutView.as_view()),
    url(r'^accounts/login', forms.LoginFormView.as_view()),

    url(r'^equip/create/$', views.create_equip),
    url(r'^equip/return/(?P<equip_id>[0-9A-Fa-f]+)/$', views.return_equip),
    url(r'^equip/remove/(?P<equip_id>[0-9A-Fa-f]+)/$', views.remove_equip),
    url(r'^equip/inc/(?P<equip_id>[0-9A-Fa-f]+)/$', views.inc_equip),
    url(r'^equip/list$', views.show_equip, name='equip_list'),
    url(r'^equip/give/(?P<equip_id>[0-9A-Fa-f]+)/$', views.give_equip),
]