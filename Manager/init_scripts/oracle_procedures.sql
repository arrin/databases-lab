-- COMPILED
-- MUST WORK, please, test
create or replace procedure InitPositions as
begin
	insert into Positions(Id, Position) values(1, 'admin');
end InitPositions;

-- COMPILED
-- MUST WORK, please, test
create or replace procedure InitPermissions(permission_id number, permission_value varchar2) as
begin
	insert into Permissions(Id, Permission) values(permission_id, permission_value);
end InitPermissions;

-- COMPILED
-- MUST WORK, please, test
create or replace procedure InitAdmin as
begin
    insert into Employees
    values (
        1,
        'admin',
        'admin',
        'admin',
        'empty',
        '111',
        (select CURRENT_DATE from DUAL),
        NULL,
        1,
        1,
        null
    );
end InitAdmin;

-- COMPILED
-- MUST WORK, please, test
create or replace function GetPositionById(pos_id number)
return Positions%ROWTYPE as
	pos_result Positions%ROWTYPE;
begin
	select * into pos_result from Positions where Id = pos_id;
	return pos_result;
end GetPositionById;

-- COMPILED
-- MUST WORK, please
create or replace function GetPositionByName(pos_name varchar2)
return Positions%ROWTYPE as
	pos_result Positions%ROWTYPE;
begin
	select * into pos_result from Positions where Position = pos_name;
	return pos_result;
exception
	when others then
		insert into Positions(Position) values(pos_name);
		select * into pos_result from Positions where Position = pos_name;
		return pos_result;
end GetPositionByName;

-- COMPILED
-- MUST WORK, please, test
create or replace procedure HireEmployee(
    nick_name varchar2,
    first_name varchar2,
    second_name varchar2,
    home_address varchar2,
    phone_number varchar2,
    position_name varchar2,
    permission_id number,
    boss_username varchar2
) as
	position Positions%ROWTYPE;
	boss Employees%ROWTYPE;
begin
	position := GetPositionByName(position_name);
	boss := GetUserByName(boss_username);

	insert into Employees
		  (NickName,  FirstName,  SecondName,  HomeAddress,   Phone,        DateHired,                            DateFired,  PositionId,  PermissionId,  BossId)
	values(nick_name, first_name, second_name, home_address,  phone_number, (select CURRENT_DATE from DUAL),      null,       position.Id, permission_id, boss.Id);
end HireEmployee;

-- COMPILED
create or replace procedure MoveEmployee(position_id varchar2, boss_id number, employee_id number) as
begin
	update Employees
	set
		PositionId   = position_id,
		BossId       = boss_id
	where
		Id = employee_id;
end MoveEmployee;

-- COMPILED
create or replace procedure FireEmployee(employee_id number) as
begin
	update Employees
	set
		DateFired = (select CURRENT_DATE from DUAL)
	where
		Id = employee_id;
end FireEmployee;

-- COMPILED
create or replace procedure AddNewEquipmentToStorage(eq_model varchar2, eq_count number) as
begin
	insert into Equipment
		  (Model,    Count)
	values(eq_model, eq_count);
end AddNewEquipmentToStorage;

-- COMPILED
create or replace procedure AddExistingEquipmentToStorage(equipment_id number, count_added number) as
begin
	update Equipment
	set
		Count = Count + count_added
	where
		Id = equipment_id;
end AddExistingEquipmentToStorage;

-- COMPILED
create or replace procedure RemoveEquipmentFromStorage(equipment_id number, count_reduced number) as
begin
	update Equipment
	set
		Count = Count - count_reduced
	where
		Id = equipment_id;
end RemoveEquipmentFromStorage;

-- COMPILED
create or replace procedure GiveEquipment(reason varchar2, equipment_id number, employee_id number) as
begin
	insert into GivenEquipment
		  ( DatetimeGiven,                   DatetimeReturned, ReasonGiven,  EquipmentId,  EmployeeId)
	values( (select CURRENT_DATE from DUAL), null,             reason,       equipment_id, employee_id);
end GiveEquipment;

-- COMPILED
create or replace procedure ReturnEquipment(equip_id number) as
begin
	update GivenEquipment
	set
		DatetimeReturned = (select CURRENT_DATE from DUAL)
	where
		Id = equip_id;
end ReturnEquipment;

-- COMPILED
-- MUST WORK, please, test
create function GetUserProfile(user_id number)
return UserExtendedObjectType as
	profile UserExtendedObjectType;
begin
	select UserExtendedObjectType(
		worker.Id,
		worker.NickName,
		worker.FirstName,
		worker.SecondName,
		worker.HomeAddress,
		worker.Phone,
		worker.DateHired,
		worker.DateFired,
		worker.BossId,
		worker.Position,
		worker.Permission
	)
	into profile
	from AllEmployeesView worker
	where worker.Id = user_id;

	return profile;
end GetUserProfile;


-- COMPILED
-- MUST WORK, please, test
create or replace function GetUserByName(username varchar2)
return Employees%ROWTYPE as
	profile Employees%ROWTYPE;
begin
	select *
	into profile
	from Employees
	where NickName = username;

	return profile;
end GetUserByName;

-- COMPILED
create or replace type EqObjectType is object (Id number, Model varchar2(100), Count number);

-- COMPILED
create or replace type EqTableType is table of EqObjectType;

-- CALL THIS by 'select * from table(FunctionName)'

-- COMPILED
-- MUST WORK, please, test
create or replace function GetAllEquipment
return EqTableType as
	eq_table EqTableType;
begin
	select EqObjectType(Id, Model, Count)
	bulk collect into eq_table
	from Equipment;

	return eq_table;
end GetAllEquipment;

-- COMPILED
create or replace type EqOwnedObjectType is object (Id number, DatetimeGiven date, Reason varchar2(100), Model varchar2(100));

-- COMPILED
create or replace type EqOwnedTableType is table of EqOwnedObjectType;

-- CALL THIS by 'select * from table(FunctionName)'

-- COMPILED
-- MUST WORK, please, test
create or replace function GetOwnedEquipment(user_id number)
return EqOwnedTableType as
	eq_table EqOwnedTableType;
begin
	select EqOwnedObjectType(
		given.Id,
		given.DatetimeGiven,
		given.ReasonGiven,
		eq.Model
    )
	bulk collect into eq_table
	from GivenEquipment given inner join Equipment eq
	on given.EquipmentId = eq.Id
	where given.EmployeeId = user_id and given.DatetimeReturned is null;

	return eq_table;
end GetOwnedEquipment;

-- COMPILED
create or replace type EqCountRestObjectType is object (Id number, Model varchar2(100), Count number, CountRest number);

-- COMPILED
create or replace type EqCountRestTableType is table of EqCountRestObjectType;

-- COMPILED
-- MUST WORK, please, test
create function GetEquipmentById(equipment_id number)
return EqCountRestObjectType as
	equip EqCountRestObjectType;
begin
	select EqCountRestObjectType(
		eq.Id,
		eq.Model,
		eq.Count,
		eq.Count - given.Count
    )
    into equip
	from Equipment eq
	left outer join GivenEquipmentGrouped given
	on eq.Id = given.EquipmentId
	where eq.Id = equipment_id;

    if (equip.CountRest is null) then
        equip.CountRest := equip.Count;
    end if;

	return equip;
end GetEquipmentById;

-- CALL THIS by 'select * from table(FunctionName)'

-- COMPILED
-- MUST WORK, please, test
create or replace function GetAllEquipmentWithCountRest
return EqCountRestTableType as
	eq_table EqCountRestTableType;
begin
	select EqCountRestObjectType(
		eq.Id,
		eq.Model,
		eq.Count,
		eq.Count - given.Count
    )
	bulk collect into eq_table
	from Equipment eq
	left outer join GivenEquipmentGrouped given
	on eq.Id = given.EquipmentId;

	return eq_table;
end GetAllEquipmentWithCountRest;

-- COMPILED
create or replace type UserObjectType is object(
    ID	NUMBER,
    NICKNAME	VARCHAR2(100),
    FIRSTNAME	VARCHAR2(100),
    SECONDNAME	VARCHAR2(100),
    HOMEADDRESS	VARCHAR2(100),
    PHONE	VARCHAR2(100),
    DATEHIRED	DATE,
    DATEFIRED	DATE,
    POSITIONID	NUMBER,
    PERMISSIONID	NUMBER,
    BOSSID	NUMBER
);

-- COMPILED
create or replace type UserTableType is table of UserObjectType;

-- CALL THIS by 'select * from table(FunctionName)'

-- COMPILED
-- MUST WORK, please, test
create or replace function GetAllUsers
return UserTableType as
	users UserTableType;
begin
	select UserObjectType(
        ID, NICKNAME, FIRSTNAME, SECONDNAME, HOMEADDRESS, PHONE,
        DATEHIRED, DATEFIRED, POSITIONID, PERMISSIONID, BOSSID
    )
	bulk collect into users
	from Employees;

	return users;
end GetAllUsers;

-- COMPILED
-- MUST WORK, please, test
create or replace type UserExtendedObjectType is object (
	Id number,
	NickName varchar2(100),
	FirstName varchar2(100),
	SecondName varchar2(100),
	HomeAddress varchar2(100),
	Phone varchar2(100),
	DateHired date,
	DateFired date,
	BossId number,
    Position varchar2(100),
	Permission varchar2(100)
);

-- COMPILED
create or replace type UserExtendedTableType is table of UserExtendedObjectType;

-- CALL THIS by 'select * from table(FunctionName)'

-- COMPILED
-- MUST WORK, please, test
create or replace function GetUsers(boss_id number)
return UserExtendedTableType as
	users UserExtendedTableType;
begin
	select UserExtendedObjectType(
		worker.Id,
		worker.NickName,
		worker.FirstName,
		worker.SecondName,
		worker.HomeAddress,
		worker.Phone,
		worker.DateHired,
		worker.DateFired,
		worker.BossId,
		worker.Position,
		worker.Permission
    )
	bulk collect into users
	from AllEmployeesView worker
	where worker.BossId = boss_id;

	return users;
end GetUsers;

-- COMPILED
create or replace type HistoryRecord is object(DatetimeCreated date, RecordContent varchar2(100));

-- COMPILED
create or replace type HistoryTable is table of HistoryRecord;

-- CALL THIS by 'select * from table(FunctionName)'

-- COMPILED
-- MUST WORK, please, test
create or replace function GetHistory(user_id number)
return HistoryTable as
	history HistoryTable;
begin
	select HistoryRecord(DatetimeCreated, RecordContent)
	bulk collect into history
	from Records
	where EmployeeId = user_id
	order by DatetimeCreated desc;

	return history;
end GetHistory;