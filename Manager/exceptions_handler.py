from django.http import HttpResponse, Http404


def exceptions_handler(func):
    def execute(*args, **kwargs):
        return func(*args, **kwargs)
        try:
            return func(*args, **kwargs)
        except Exception:
            raise Http404

    return execute

