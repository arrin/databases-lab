-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure AddExistingEquipmentToStorageWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    equipment_id number;
    count_added number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.equipment_id') into equipment_id from dual;
    select JSON_VALUE(args_in_json, '$.count_added') into count_added from dual;

    ADDEXISTINGEQUIPMENTTOSTORAGE(equipment_id, count_added);
end AddExistingEquipmentToStorageWrapper;

-- test
begin
    AddExistingEquipmentToStorageWrapper('{"equipment_id" : 1, "count_added" : 50}', 1, 'strannik');
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure AddNewEquipmentToStorageWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    eq_model varchar2(1000);
    eq_count number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.eq_model') into eq_model from dual;
    select JSON_VALUE(args_in_json, '$.eq_count') into eq_count from dual;

    ADDNEWEQUIPMENTTOSTORAGE(eq_model, eq_count);
end;

-- test
begin
    AddNewEquipmentToStorageWrapper('{"eq_model" : "HyperX Pulsefire Surge", "eq_count" : 50}', 1, 'strannik');
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure FireEmployeeWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    employee_id number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.employee_id') into employee_id from dual;

    FIREEMPLOYEE(employee_id);
end;

-- test
begin
    FireEmployeeWrapper('{"employee_id" : 23 }', 1, 'strannik');
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure GiveEquipmentWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    reason varchar2(1000);
    equipment_id number;
    employee_id number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.reason') into reason from dual;
    select JSON_VALUE(args_in_json, '$.equipment_id') into equipment_id from dual;
    select JSON_VALUE(args_in_json, '$.employee_id') into employee_id from dual;

    GIVEEQUIPMENT(reason, equipment_id, employee_id);
end;

-- test
begin
    GiveEquipmentWrapper('{"reason" : "Razdayu equipment", "equipment_id" : 1, "employee_id" : 1}', 1, 'strannik');
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure HireEmployeeWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    nick_name varchar2(1000);
    first_name varchar2(1000);
    second_name varchar2(1000);
    home_address varchar2(1000);
    phone_number varchar2(1000);
    position_name varchar2(1000);
    permission_id number;
    boss_username varchar2(1000);
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.nick_name') into nick_name from dual;
    select JSON_VALUE(args_in_json, '$.first_name') into first_name from dual;
    select JSON_VALUE(args_in_json, '$.second_name') into second_name from dual;
    select JSON_VALUE(args_in_json, '$.home_address') into home_address from dual;
    select JSON_VALUE(args_in_json, '$.phone_number') into phone_number from dual;
    select JSON_VALUE(args_in_json, '$.position_name') into position_name from dual;
    select JSON_VALUE(args_in_json, '$.permission_id') into permission_id from dual;
    select JSON_VALUE(args_in_json, '$.boss_username') into boss_username from dual;

    HIREEMPLOYEE(nick_name, first_name, second_name, home_address, phone_number, position_name, permission_id, boss_username);
end;

-- test
begin
    HireEmployeeWrapper(
        '{' ||
        ' "nick_name" : "NICK", ' ||
        ' "first_name" : "NICK", ' ||
        ' "second_name" : "NICK", ' ||
        ' "home_address" : "HOME",' ||
        ' "phone_number" : "PHONE",' ||
        ' "position_name" : "NEW_POS",' ||
        ' "permission_id" : 1,' ||
        ' "boss_username" : "admin"' ||
        '}',
        1, 'strannik'
    );
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure MoveEmployeeWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    position_id varchar2(1000);
    boss_id number;
    employee_id number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.position_id') into position_id from dual;
    select JSON_VALUE(args_in_json, '$.boss_id') into boss_id from dual;
    select JSON_VALUE(args_in_json, '$.employee_id') into employee_id from dual;

    MOVEEMPLOYEE(position_id, boss_id, employee_id);
end;

-- test
begin
    MoveEmployeeWrapper('{"position_id" : 1, "boss_id" : 1, "employee_id" : 61}', 1, 'strannik');
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure RemoveEquipmentFromStorageWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    equipment_id number;
    count_reduced number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.equipment_id') into equipment_id from dual;
    select JSON_VALUE(args_in_json, '$.count_reduced') into count_reduced from dual;

    REMOVEEQUIPMENTFROMSTORAGE(equipment_id, count_reduced);
end;

-- test
begin
    RemoveEquipmentFromStorageWrapper('{"equipment_id" : 1, "count_reduced" : 25}', 1, 'strannik');
end;

-- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER -- NEXT WRAPPER --

create or replace procedure ReturnEquipmentWrapper(args_in_json varchar2, user_id number, user_nickname varchar2) as
    equip_id number;
begin
    SET_CURRENT_USER_ID(user_id);
    SET_CURRENT_USER_NICKNAME(user_nickname);

    select JSON_VALUE(args_in_json, '$.equip_id') into equip_id from dual;

    RETURNEQUIPMENT(equip_id);
end;

-- test
begin
    ReturnEquipmentWrapper('{"equip_id" : 41}', 1, 'strannik');
end;