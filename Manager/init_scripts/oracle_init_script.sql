create table Positions(
	Id       integer       generated by default on null as identity primary key,
	Position varchar2(100) not null unique
);

create table Permissions(
	Id         integer       generated by default on null as identity primary key,
	Permission varchar2(100) not null unique
);

create table Employees(
	Id           integer       generated by default on null as identity primary key,
	NickName     varchar2(100) not null unique,
	FirstName    varchar2(100) not null,
	SecondName   varchar2(100) not null,
	HomeAddress  varchar2(100)     null,
	Phone        varchar2(100)     null,
	DateHired    date          not null,
	DateFired    date              null,
	PositionId   integer       not null,
	PermissionId integer       not null,
	BossId       integer       null,

	foreign key(PositionId)   references Positions(Id),
	foreign key(PermissionId) references Permissions(Id),
	foreign key(BossId)       references Employees(Id)
);

create table Equipment(
	Id    integer       generated by default on null as identity primary key,
	Model varchar2(100) not null unique,
	Count integer       not null
);

create table GivenEquipment(
	Id               integer       generated by default on null as identity primary key,
	DatetimeGiven    date          not null,
	DatetimeReturned date              null,
	ReasonGiven      varchar2(100) not null,
	EquipmentId      integer       not null,
	EmployeeId       integer       not null,

	foreign key(EquipmentId) references Equipment(Id),
	foreign key(EmployeeId)  references Employees(Id)
);

create table Records(
	Id              integer       generated by default on null as identity primary key,
	RecordContent   varchar2(100) not null,
	DatetimeCreated date          not null,
	EmployeeId      integer       not null,

	foreign key(EmployeeId) references Employees(Id)
);

create view AllEmployeesView as
select
	worker.Id              as Id,
	worker.NickName        as NickName,
	worker.FirstName       as FirstName,
	worker.SecondName      as SecondName,
	worker.HomeAddress     as HomeAddress,
	worker.Phone           as Phone,
	worker.DateHired       as DateHired,
	worker.DateFired       as DateFired,
	worker.BossId          as BossId,

	pos.Position           as Position,
	perm.Permission        as Permission
from Employees worker
	inner join Positions pos
	on worker.PositionId = pos.Id
	inner join Permissions perm
	on worker.PermissionId = perm.Id;

create view GivenEquipmentGrouped as
select
  EquipmentId,
  count(*) as Count

from GivenEquipment

where DatetimeReturned is null

group by EquipmentId;