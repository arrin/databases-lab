create or replace type QueueType as object(
    ProcName varchar2(1000),
    ArgsJson varchar2(1000),
    UserId number,
    UserName varchar2(1000)
);

begin
    dbms_aqadm.create_queue_table('AsyncQueueTable', 'QueueType', multiple_consumers => true);
end;

begin
    dbms_aqadm.create_queue('AsyncQueue', 'AsyncQueueTable');
end;

begin
    dbms_aqadm.start_queue('AsyncQueue');
end;

select * from USER_QUEUES;

-- LOG TABLE FOR THE QUEUE
create table QueueLog(
    AddTime timestamp default systimestamp not null,
    Msg varchar2(4000)
);

-- PACKAGE HEADER
create or replace package QueuePackage is
    procedure AddLog(msg_arg in QueueLog.Msg%TYPE);

    procedure ProcessMessage(async_call QueueType);

    procedure Subscriber(context raw, reginfo sys.aq$_reg_info,
      descr sys.aq$_descriptor, payload raw, payloadl number);

end QueuePackage;

-- PACKAGE BODY
create or replace package body QueuePackage is

  procedure AddLog(msg_arg in QueueLog.Msg%type) as
  begin
    insert into QueueLog(Msg) values(msg_arg);
  end AddLog;

  procedure ProcessMessage(async_call QueueType) as
  begin
    case async_call.ProcName
    when 'AddExistingEquipmentToStorage' then
        AddExistingEquipmentToStorageWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'AddNewEquipmentToStorage' then
        AddNewEquipmentToStorageWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'FireEmployee' then
        FireEmployeeWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'GiveEquipment' then
         GiveEquipmentWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'HireEmployee' then
        HireEmployeeWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'MoveEmployee' then
        MoveEmployeeWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'RemoveEquipmentFromStorage' then
        RemoveEquipmentFromStorageWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    when 'ReturnEquipment' then
        ReturnEquipmentWrapper(async_call.ARGSJSON, async_call.USERID, async_call.USERNAME);
    end case;
  end;

  procedure Subscriber(context raw, reginfo sys.aq$_reg_info,
    descr sys.aq$_descriptor, payload raw, payloadl number)
  as
    queue_message QueueType;
    l_msg_props   dbms_aq.message_properties_t;
    l_queue_opts  dbms_aq.dequeue_options_t;
    l_msg_id      raw(16);
  begin
    l_queue_opts.consumer_name := descr.consumer_name;
    l_queue_opts.msgid := descr.msg_id;
    dbms_aq.dequeue(descr.queue_name, l_queue_opts, l_msg_props, queue_message, l_msg_id);


    -- MESSAGE PROCESSING
    ProcessMessage(queue_message);

    -- ADDITIONAL LOGGING
    AddLog('User #' || queue_message.UserId ||
           ' "' || queue_message.UserName ||
           '" called proc "' || queue_message.ProcName || '";'
    );
  exception
  when others then
    AddLog(sqlerrm);
  end Subscriber;

end QueuePackage;

-- HOW TO CREATE SUBSCRIBER
begin
  dbms_aqadm.add_subscriber('AsyncQueue', sys.aq$_agent('test_subscriber', null, null));
  dbms_aq.register(sys.aq$_reg_info_list(sys.aq$_reg_info('AsyncQueue:test_subscriber', dbms_aq.namespace_aq, 'plsql://QueuePackage.Subscriber', hextoraw('FF'))), 1);
end;

-- HOW TO DELETE SUBSCRIBER
declare
   subscriber       sys.aq$_agent;
begin
   subscriber := sys.aq$_agent('test_subscriber','AsyncQueue', null);
   dbms_aqadm.REMOVE_SUBSCRIBER(
      queue_name => 'AsyncQueue',
      subscriber => subscriber);
end;

-- HOW TO ENQUEUE MESSAGE
declare
  l_msg_props   dbms_aq.message_properties_t;
  l_queue_opts  dbms_aq.enqueue_options_t;
  l_msg_id      raw(16);
begin
  dbms_aq.enqueue('AsyncQueue', l_queue_opts, l_msg_props, QueueType('Proc', 'Args', 1, 'strannik'), l_msg_id);
  commit;
end;

-- PROCEDURE TO PUSH MESSAGE
create or replace procedure PushMessage(proc_name varchar2, args_json varchar2, user_id number, user_name varchar2) as
  l_msg_props   dbms_aq.message_properties_t;
  l_queue_opts  dbms_aq.enqueue_options_t;
  l_msg_id      raw(16);
begin
  dbms_aq.enqueue('AsyncQueue', l_queue_opts, l_msg_props, QueueType(proc_name, args_json, user_id, user_name), l_msg_id);
end;

-- test
begin
    PushMessage('FireEmployee', '{"employee_id" : 61}', 1, 'strannik');
end;