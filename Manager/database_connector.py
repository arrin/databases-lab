from Manager.oracle_connector_new import Storage
from Manager.entities import User
from Manager.entities import Equipment
from Manager.entities import UserEquipment
from Manager.entities import Permissions
from Manager.entities import Record

def _convert_to_user(user_obj):
    db = Storage().get()
    boss_id = user_obj.BOSSID

    pos_id = None
    pos_name = None
    try:
        pos_name = user_obj.POSITION
    except:
        pos_id = user_obj.POSITIONID
        pos_name = db.GetPositionById(pos_id).POSITION

    perm_id = None
    perm_name = None
    try:
        perm_name = user_obj.PERMISSION
    except:
        perm_id = user_obj.PERMISSIONID
        perm_name = Permissions(perm_id)

    # print(user_obj.ID)
    return User(
            id=user_obj.ID,
            username=user_obj.NICKNAME,
            first_name=user_obj.FIRSTNAME,
            second_name=user_obj.SECONDNAME,
            home_address=user_obj.HOMEADDRESS,
            phonenumber=user_obj.PHONE,
            date_hired=user_obj.DATEHIRED,
            date_fired=user_obj.DATEFIRED,
            position=pos_name,
            permissions=perm_name,
            boss_username=(db.GetUserProfile(boss_id).NICKNAME if boss_id is not None
                           else None)
        )


def GetUserProfile(user_id):
    user_id = int(user_id)
    db = Storage().get()
    raw = db.GetUserProfile(user_id)

    return _convert_to_user(raw)


def GetUserHistory(user_id):
    user_id = int(user_id)
    db = Storage().get()

    raw_list = db.GetHistory(user_id)
    result = list(map(lambda raw: Record(raw.DATETIMECREATED, raw.RECORDCONTENT),
                  raw_list))

    return result


def HireUser(user, w_id, worker_username):
    user.permissions = user.permissions.value

    db = Storage().get()
    boss_name = user.boss_username if user.boss_username is not None else 'admin'
    # pos_id = db.GetPositionByName(user.position).ID

    db.HireEmployeePutMessage(user.username, user.first_name, user.second_name,
                              user.home_address, user.phonenumber, user.position,
                              user.permissions, boss_name, w_id, worker_username)



def MoveUser(user, w_id, worker_username):
    # user.position = user.permissions.value
    db = Storage().get()
    pos_id = db.GetPositionByName(user.position).ID
    boss_name = user.boss_username if user.boss_username is not None else 'admin'
    boss_id = db.GetUserByName(boss_name).ID

    db.MoveEmployeePutMessage(pos_id, boss_id, user.id, w_id, worker_username)


def FireUser(user_id, w_id, worker_username):
    user_id = int(user_id)
    db = Storage().get()

    db.FireEmployeePutMessage(user_id, w_id, worker_username)


def GetUsers(boss_id = None):
    boss_id = int(boss_id) if boss_id is not None else None
    db = Storage().get()

    if (boss_id is None):
        res = db.GetAllUsers()
    else:
        res = db.GetUsers(boss_id)
    result = []
    for item in res:
        result.append(_convert_to_user(item))
    return result


def GetUserProfileByUsername(username):
    db = Storage().get()
    obj = db.GetUserByName(username)
    return _convert_to_user(obj)


def CreateEquipment(equipment, w_id, worker_username):
    db = Storage().get()

    db.AddNewEquipmentToStoragePutMessage(equipment.model, equipment.count,
                                          w_id, worker_username)


def GiveEquipment(equip_id, user_id, reason, w_id, worker_username):
    equip_id = int(equip_id)
    user_id = int(user_id)
    db = Storage().get()

    eq = GetEquipment(equip_id)
    if eq.count_free == 0:
        return

    db.GiveEquipmentPutMessage(equip_id, user_id, reason,
                               w_id, worker_username)


def ReturnEquipment(equip_id, w_id, worker_username):
    equip_id = int(equip_id)
    db = Storage().get()

    db.ReturnEquipmentPutMessage(equip_id, w_id, worker_username)


def RemoveEquipment(equip_id, w_id, worker_username):
    equip_id = int(equip_id)
    db = Storage().get()

    db.RemoveEquipmentFromStoragePutMessage(equip_id, 1, w_id, worker_username)


def IncrementEquipment(equip_id, w_id, worker_username):
    equip_id = int(equip_id)
    db = Storage().get()

    db.AddExistingEquipmentToStoragePutMessage(equip_id, 1, w_id,
                                               worker_username)


def _convert_equip(obj):
    eq = Equipment(id=obj.ID, model=obj.MODEL,
                   count=obj.COUNT, count_free=obj.COUNTREST)
    return eq


def GetEquipment(equip_id):
    equip_id = int(equip_id)
    db = Storage().get()
    obj = db.GetEquipmentById(equip_id)
    return _convert_equip(obj)


def GetAllEquipment():
    db = Storage().get()
    raw_list = db.GetAllEquipmentWithCountRest()

    equipment_list = list(map(lambda obj: _convert_equip(obj), raw_list))
    return equipment_list


def GetOwnedEquipment(user_id):
    user_id = int(user_id)
    db = Storage().get()
    raw_list = db.GetOwnedEquipment(user_id)

    user_equipment_list = list(map(lambda obj:
            UserEquipment(id=obj.ID, name=obj.MODEL, reason=obj.REASON, date_given=obj.DATETIMEGIVEN),
            raw_list))

    return user_equipment_list

