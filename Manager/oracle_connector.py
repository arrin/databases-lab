import cx_Oracle
import Manager.entities as models

class Storage:
    instance = None

    def __init__(self):
        if Storage.instance is None:
            host = 'localhost'
            port = 1521
            SID = 'XE'
            username = 'C##TESTER2'
            userpass = 'TESTER2'

            Storage.instance = OracleDatabase(host, port, SID, username, userpass)
            Storage.instance.init_data()

    def get(self):
        return Storage.instance


class OracleDatabase:
    def __init__(self, host, port, SID, username, userpass):
        dsn_tns = cx_Oracle.makedsn(host, port, SID)
        conn = cx_Oracle.connect(username, userpass, dsn_tns)
        cur = conn.cursor()

        self.POSITIONS_TYPE = conn.gettype('POSITIONS%ROWTYPE')
        self.EMPLOYEES_TYPE = conn.gettype('EMPLOYEES%ROWTYPE')
        self.EQUIPMENT_TYPE = conn.gettype('EQOBJECTTYPE')
        self.EQUIPMENT_TABLE_TYPE = conn.gettype('EQTABLETYPE')
        self.EQUIPMENT_OWNED_TYPE = conn.gettype('EQOWNEDOBJECTTYPE')
        self.EQUIPMENT_OWNED_TABLE_TYPE = conn.gettype('EQOWNEDTABLETYPE')
        self.EQUIPMENT_COUNT_REST_TYPE = conn.gettype('EQCOUNTRESTOBJECTTYPE')
        self.EQUIPMENT_COUNT_REST_TABLE_TYPE = conn.gettype('EQCOUNTRESTTABLETYPE')
        self.USER_TYPE = conn.gettype('USEROBJECTTYPE')
        self.USER_TABLE_TYPE = conn.gettype('USERTABLETYPE')
        self.USER_EXTENDED_TYPE = conn.gettype('USEREXTENDEDOBJECTTYPE')
        self.USER_EXTENDED_TABLE_TYPE = conn.gettype('USEREXTENDEDTABLETYPE')
        self.HISTORY_RECORD_TYPE = conn.gettype('HISTORYRECORD')
        self.HISTORY_RECORD_TABLE_TYPE = conn.gettype('HISTORYTABLE')

        self.connection = conn
        self.cursor = cur
        self.inited = True

    def init_data(self):
        if self.inited is True:
            return

        items = [(p.value, p.name.lower()) for p in models.Permissions]

        self.cursor.callproc('INITPOSITIONS')

        for it in items:
            self.cursor.callproc(
                'INITPERMISSIONS',
                parameters=it
            )

        self.cursor.callproc('INITADMIN')
        self.connection.commit()

    def GetPositionById(self, id):
        result = self.cursor.callfunc(
            'GETPOSITIONBYID',
            self.POSITIONS_TYPE,
            parameters=[id]
        )

        return result

    def GetPositionByName(self, name):
        result = self.cursor.callfunc(
            'GETPOSITIONBYNAME',
            self.POSITIONS_TYPE,
            parameters=[name]
        )

        self.connection.commit()

        return result

    def HireEmployee(self, nickname, first_name, second_name, home_address,
                     phone_number, position_id, permission_id, boss_id):
        self.cursor.callproc(
            'HIREEMPLOYEE',
            parameters=[
                nickname, first_name, second_name, home_address,
                phone_number, position_id, permission_id, boss_id
            ]
        )

        self.connection.commit()

    def MoveEmployee(self, position_id, boss_id, employee_id):
        self.cursor.callproc(
            'MOVEEMPLOYEE',
            parameters=[position_id, boss_id, employee_id]
        )

        self.connection.commit()

    def FireEmployee(self, id):
        self.cursor.callproc(
            'FIREEMPLOYEE',
            parameters=[id]
        )

        self.connection.commit()

    def AddNewEquipmentToStorage(self, model, count):
        self.cursor.callproc(
            'ADDNEWEQUIPMENTTOSTORAGE',
            parameters=[model, count]
        )

        self.connection.commit()

    def AddExistingEquipmentToStorage(self, equipment_id, count_added):
        self.cursor.callproc(
            'ADDEXISTINGEQUIPMENTTOSTORAGE',
            parameters=[equipment_id, count_added]
        )

        self.connection.commit()

    def RemoveEquipmentFromStorage(self, equipment_id, count_reduced):
        self.cursor.callproc(
            'REMOVEEQUIPMENTFROMSTORAGE',
            parameters=[equipment_id, count_reduced]
        )

        self.connection.commit()

    def GiveEquipment(self, equipment_id, employee_id, reason):
        self.cursor.callproc(
            'GIVEEQUIPMENT',
            parameters=[reason, equipment_id, employee_id]
        )

        self.connection.commit()

    def ReturnEquipment(self, given_equipment_id):
        self.cursor.callproc(
            'RETURNEQUIPMENT',
            parameters=[given_equipment_id]
        )

        self.connection.commit()

    def GetUserProfile(self, user_id):
        result = self.cursor.callfunc(
            'GETUSERPROFILE',
            self.USER_EXTENDED_TYPE,
            parameters=[user_id]
        )

        return result

    def GetUserByName(self, username):
        result = self.cursor.callfunc(
            'GETUSERBYNAME',
            self.EMPLOYEES_TYPE,
            parameters=[username]
        )

        return result

    def GetAllEquipment(self):
        result = self.cursor.callfunc(
            'GETALLEQUIPMENT',
            self.EQUIPMENT_TABLE_TYPE
        )

        return result.aslist()

    def GetEquipmentById(self, eq_id):
        result = self.cursor.callfunc(
            'GETEQUIPMENTBYID',
            self.EQUIPMENT_COUNT_REST_TYPE,
            parameters=[eq_id]
        )

        return result

    def GetOwnedEquipment(self, user_id):
        result = self.cursor.callfunc(
            'GETOWNEDEQUIPMENT',
            self.EQUIPMENT_OWNED_TABLE_TYPE,
            parameters=[user_id]
        )

        return result.aslist()

    def GetAllEquipmentWithCountRest(self):
        result = self.cursor.callfunc(
            'GETALLEQUIPMENTWITHCOUNTREST',
            self.EQUIPMENT_COUNT_REST_TABLE_TYPE
        )

        return result.aslist()

    def GetAllUsers(self):
        result = self.cursor.callfunc(
            'GETALLUSERS',
            self.USER_TABLE_TYPE
        )

        return result.aslist()

    def GetUsers(self, boss_id):
        result = self.cursor.callfunc(
            'GETUSERS',
            self.USER_EXTENDED_TABLE_TYPE,
            parameters=[boss_id]
        )

        return result.aslist()

    def GetHistory(self, user_id):
        result = self.cursor.callfunc(
            'GETHISTORY',
            self.HISTORY_RECORD_TABLE_TYPE,
            parameters=[user_id]
        )

        return result.aslist()


def test_main():
    host = 'localhost'
    port = 1521
    SID = 'XE'
    username = 'C##TESTER2'
    userpass = 'C##TESTER2'

    db = OracleDatabase(host, port, SID, username, userpass)
    db.init_data()

    pos = db.GetPositionById(1)
    print(pos.ID, ' : ', pos.POSITION)

    pos = db.GetPositionByName('new_position')
    print(pos.ID, ' : ', pos.POSITION)

    db.HireEmployee('ilan2281488', 'Ilan', '...', 'L. Byadi, 4', '+375297217601', 1, 1, 'admin')
    # db.MoveEmployee(1, 1, 2)
    # db.FireEmployee(1)
    # db.AddNewEquipmentToStorage('HyperX Alloy Elite RGB', 15)
    # db.AddExistingEquipmentToStorage(1, 15)
    # db.RemoveEquipmentFromStorage(1, 10)
    # db.GiveEquipment(1, 1, 'Nothing')
    # db.ReturnEquipment(1)

    user = db.GetUserProfile(1)
    print(user.ID, ' : ', user.NICKNAME)

    user = db.GetUserByName('admin')
    print(user.ID, ' : ', user.NICKNAME)

    for eq in db.GetAllEquipment():
        print(eq.ID, ' : ', eq.MODEL)

    eq = db.GetEquipmentById(1)
    print(eq.ID, ' : ', eq.MODEL, eq.COUNTREST)

    for eq in db.GetOwnedEquipment(1):
        print(eq.ID, ' : ', eq.MODEL, ' : ', eq.REASON)

    for eq in db.GetAllEquipmentWithCountRest():
        print(eq.ID, ' : ', eq.MODEL, ' : ', eq.COUNTREST)

    for user in db.GetAllUsers():
        print(user.ID, ' -- ', user.FIRSTNAME, ' ', user.SECONDNAME)

    for user in db.GetUsers(1):
        print(user.ID, ' -- ', user.NICKNAME, ' <-- ', user.BOSSID)

    for record in db.GetHistory(1):
        print(record.RECORDCONTENT)

host = 'localhost'
port = 1521
SID = 'XE'
username = 'C##TESTER2'
userpass = 'TESTER2'

db = OracleDatabase(host, port, SID, username, userpass)
# db.HireEmployee('ilan2281488', 'Ilan', '...', 'L. Byadi, 4',
#                 '+375297217601', 1, 1, 'admin')