from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.forms import Form
from django import forms
from Manager.entities import Permissions

class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login.html"
    success_url = "/actions"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        super(LoginFormView, self).form_valid(form)
        return HttpResponseRedirect(self.success_url)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class EditPersonForm(Form):
    username = forms.CharField(max_length=50, disabled=True, required=False)
    first_name = forms.CharField(max_length=50, disabled=True, required=False)
    second_name = forms.CharField(max_length=50, disabled=True, required=False)
    home_address = forms.CharField(max_length=50, disabled=True, required=False)
    phonenumber = forms.CharField(max_length=50, disabled=True, required=False)
    permissions = forms.CharField(max_length=50, disabled=True, required=False)
    position = forms.CharField(max_length=50, required=False)
    boss_username = forms.CharField(max_length=50, required=False)
    date_hired = forms.DateField(disabled=True, widget=forms.SelectDateWidget, required=False)


class CreatePersonForm(Form):
    username = forms.CharField(max_length=50)
    first_name = forms.CharField(max_length=50)
    second_name = forms.CharField(max_length=50)
    home_address = forms.CharField(max_length=50)
    phonenumber = forms.CharField(max_length=50, required=False)
    position = forms.CharField(max_length=50, required=False)
    boss_username = forms.CharField(max_length=50, required=False)
    status = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select-wrapper'}),
                               choices=[(p.value, p.name.lower()) for p in Permissions],
                               required=False)


class ViewPersonForm(Form):
    username = forms.CharField(max_length=50, disabled=True)
    first_name = forms.CharField(max_length=50, disabled=True)
    second_name = forms.CharField(max_length=50, disabled=True)
    home_address = forms.CharField(max_length=50, disabled=True)
    phonenumber = forms.CharField(max_length=50, disabled=True)
    permissions = forms.CharField(max_length=50, disabled=True)
    position = forms.CharField(max_length=50, disabled=True)
    boss_username = forms.CharField(max_length=50, disabled=True)
    date_hired = forms.DateField(disabled=True, widget=forms.SelectDateWidget)
    date_fired = forms.DateField(disabled=True, widget=forms.SelectDateWidget)


class EquipmentForm(Form):
    name = forms.CharField(max_length=50)
    count = forms.IntegerField()


class GiveEquipmentForm(Form):
    username = forms.CharField(max_length=50)
    reason = forms.CharField(max_length=50)